import os
import csv
import json
import tweepy


class TweetMiner(object):
    # connect twitter api

    HOME_PATH = os.path.dirname(__file__)

    twitter_auth_data = open(os.path.abspath(os.path.join(HOME_PATH, "..", "twitter_auth_data.json"))).read()
    twitter_auth_data_json = json.loads(twitter_auth_data)

    access_token = twitter_auth_data_json["access_token"]
    access_token_secret = twitter_auth_data_json["access_token_secret"]
    consumer_key = twitter_auth_data_json["consumer_key"]
    consumer_secret = twitter_auth_data_json["consumer_secret"]

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)


    def __init__(self, screen_name):

        self.screen_name = screen_name
        self.api = tweepy.API(TweetMiner.auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True, retry_count=10, retry_delay=5, retry_errors=set([503]))


    def get_user_profile(self):

        user = self.api.get_user(self.screen_name)

        user_data = {
            "username": user.name,
            "screen_name": user.screen_name,
            "info": user.description,
            "total_posts": user.statuses_count,
            "followers": user.followers_count,
            "following": user.friends_count,
            "liked": user.favourites_count,
            "image": user.profile_image_url,
            "location": user.location,
            "url": user.url,
            "created": user.created_at.strftime("%Y-%m-%d"),
        }

        with open(self.dir_profile_csv, "w") as f:
            w = csv.DictWriter(f, user_data.keys())
            w.writeheader()
            w.writerow(user_data)

    def extract_hashtags(self):
        pass
    def save_to_csv(self):
        pass
    def scrape_tweets(self):
        pass
    def get_tweets_via_api(self):
        pass
    def follower_growth(self):
        pass
    def get_followers_list(self):
        pass
    def get_followers_ids(self):
        pass
    def get_followers_ids_to_list(self):
        pass
    def fetch_user_profile(self):
        pass
    def fetch_tweet_stats(self):
        pass
    def follower_growth_to_chart(self):
        pass
    def follower_loc_to_map(self):
        pass
    def geocoding(self):
        pass
    def geocode_to_json(self):
        pass
    def get_map_data_from_json(self):
        pass
    def get_model_results(self):
       pass

if __name__ == "__main__":

    TweetMiner('POTUS').get_user_profile()
